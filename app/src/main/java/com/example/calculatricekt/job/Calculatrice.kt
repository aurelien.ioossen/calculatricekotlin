package com.example.calculatricekt.job

import android.view.View
import android.widget.Button
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode
import java.nio.channels.OverlappingFileLockException
import kotlin.concurrent.thread

open public class Calculatrice {

    private var afficheur = ""
    private var afficheurSecondaire = ""
    private var resultatOfCalcul = ""
    private var operandeFirst = ""
    private var operandeSecond = ""
    private var operateur = ""
    private var listOfOperator = listOf<String>("+", "-", "/", "*")


    fun presserTouche(textOfButton: String): String {

        when (textOfButton) {

            "AC" -> aCisAsked(textOfButton)

        }

        updateAfficheur(textOfButton)

        checkIfbuttonIsOperator(textOfButton)

        checkifResultAsked(textOfButton)


        return afficheur


    }

    private fun aCisAsked(textOfButton: String): Boolean {
        var isReset = false
        if (textOfButton == "AC") {
            reinitialiseAC()
            isReset = true
        }
        return isReset
    }

    private fun reinitialiseAC() {
        println("réinitialisation ")
        operandeFirst = ""
        operandeSecond = ""
        operateur = ""
        afficheur = ""
        afficheurSecondaire = ""
        resultatOfCalcul = ""


    }

    private fun checkifResultAsked(textOfButton: String) {

        if (checkIfTextIsEquals(textOfButton)) lunchCalcul(false)
    }


    private fun checkifIsAnotherOperateur(textOfButton: String): Boolean {
        var isAnotherOperator = false
        if (operateur.isNotEmpty()) {
            println("operateur non vide")
            if (listOfOperator.contains(textOfButton)) {
                println("autre operateur detecté")
                isAnotherOperator = true
            }
        }
        return isAnotherOperator
    }


    private fun checkIfTextIsEquals(textOfButton: String): Boolean {

        if (textOfButton == "=") {
            return true
        } else {
            return false
        }
    }


    private fun lunchCalcul(isSecondOperator: Boolean) {
        var idDividedByZero = false
        if (afficheur.isNotEmpty()) {


            operandeSecond =
                afficheur.substring(afficheur.indexOf(operateur) + 1, afficheur.length - 1)
            println("calcul...")
            println("operande 1:$operandeFirst operande2: $operandeSecond")
            idDividedByZero = calcul()
        }

        println("Resultat = $resultatOfCalcul")
        if (isSecondOperator) {

            afficheurSecondaire = if (idDividedByZero) "ERREUR /0 !" else resultatOfCalcul
            afficheur = ""
        } else {
            afficheur = resultatOfCalcul
            afficheurSecondaire = if (idDividedByZero) "ERREUR /0 !" else ""
            operandeFirst = resultatOfCalcul
            operateur = ""
            operandeSecond = ""
        }


    }


    private fun calcul():Boolean {
        var idDividedByZero = false

        when (operateur) {

            "+" -> resultatOfCalcul =
                (operandeFirst.toBigDecimal() + operandeSecond.toBigDecimal()).stripTrailingZeros()
                    .toPlainString()
            "-" -> resultatOfCalcul =
                (operandeFirst.toBigDecimal() - operandeSecond.toBigDecimal()).stripTrailingZeros()
                    .toPlainString()
            "*" -> resultatOfCalcul =
                (operandeFirst.toBigDecimal() * operandeSecond.toBigDecimal()).stripTrailingZeros()
                    .toPlainString()
            "/" -> idDividedByZero = calculDivision(idDividedByZero)

        }
        return idDividedByZero
    }

    private fun calculDivision(idDividedByZero: Boolean): Boolean {
        var idDividedByZero1 = idDividedByZero
        try {
            resultatOfCalcul =
                (operandeFirst.toBigDecimal().divide(
                    operandeSecond.toBigDecimal(),
                    10,
                    RoundingMode.HALF_EVEN
                )).stripTrailingZeros().toPlainString()
        } catch (e: Exception) {
            resultatOfCalcul = ""
            reinitialiseAC()
            idDividedByZero1 = true
        }
        return idDividedByZero1
    }

    private fun checkIfbuttonIsOperator(textOfButton: String) {
        if (!(afficheur.isEmpty() && (textOfButton in ("+-*/")))) {

            if (listOfOperator.contains(textOfButton) && operateur.isEmpty()) {
                println("Opérateur détecté : $textOfButton")
                operateur = textOfButton
                operandeFirst = afficheur.substring(0, afficheur.indexOf(operateur))
                println("Première Opérande :$operandeFirst")
                println("opérateur : $operateur")
            } else if (listOfOperator.contains(textOfButton) && operateur.isNotEmpty()) {
                println("Nouvel opérateur détecté")
                checkIfNeededToChangeOperator(textOfButton)

            }
        }
    }


    fun checkIfNeededToChangeOperator(textOfButton: String) {

        if (listOfOperator.contains(
                afficheur.substring(
                    afficheur.indexOf(operateur) + 1,
                    afficheur.indexOf(operateur) + 2
                )
            )
        ) {
            println("remplacement de l'opérateur")
            println("afficheur actuel $afficheur")
            afficheur = afficheur.substring(0, afficheur.indexOf(operateur)) + textOfButton
            operateur = textOfButton

            println("nouvel afficheur $afficheur")
        } else {
            lunchCalcul(true)
            operateur = textOfButton
            operandeFirst = resultatOfCalcul
            afficheur = "$operateur"
        }
    }

    private fun updateAfficheur(textOfButton: String) {

        if ((afficheur.isNotEmpty() || (textOfButton !in ("=.+-*/")))) {

            if (!checkIfTextIsPointAndNotInAnotherOperande(
                    textOfButton
                )
            ) {

                println("Touche $textOfButton pressée")
                afficheur += textOfButton
                println("nouvelle afficheur : $afficheur")
            }
        }
    }

    private fun afficheurContientOperateur(): Boolean {

        var isOperateurAlreadyDefined = false;

        for (operator in listOfOperator) {
            if (afficheur.contains(operator)) {
                isOperateurAlreadyDefined = true
            }
        }
        return isOperateurAlreadyDefined
    }


    private fun checkIfTextIsPointAndNotInAnotherOperande(textOfButton: String): Boolean {
        var isAnotherpointInOperande = false

        if (textOfButton == ".") {
            println(". détecté -> test de doublons")

            isAnotherpointInOperande = checkInOperande(isAnotherpointInOperande)

        }

        return isAnotherpointInOperande
    }

    private fun checkInOperande(isAnotherpointInOperande: Boolean): Boolean {
        var isAnotherpointInOperande1 = isAnotherpointInOperande
        if (operateur.isEmpty()) {
            println("operateur vide -> test 1er opérande")
            if (afficheur.contains(".")) {
                isAnotherpointInOperande1 = true
                println("1er operande contient dejà un point")
            }
        } else {
            if (afficheur.substring(afficheur.indexOf(operateur)).contains(".")) {
                println("2eme opérande contient déjà un point")
                isAnotherpointInOperande1 = true
            }

        }
        return isAnotherpointInOperande1
    }


    fun getAfficheurSecondaire(): String {

        return afficheurSecondaire

    }


}
