package com.example.calculatricekt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.calculatricekt.job.Calculatrice
import kotlinx.android.synthetic.main.activity_main.*

class Calculatrice : AppCompatActivity(), View.OnClickListener {
    var calculatrice = Calculatrice()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onClick(v: View?) {

        val btnTyped = v?.findViewById<Button>(v?.id)
        var afficheurUpdated = calculatrice.presserTouche(btnTyped?.text.toString())

        tvAfficheur.text = afficheurUpdated
        tvAfficheurSecondaire.text = calculatrice.getAfficheurSecondaire()

    }


    override fun onRestart() {
        super.onRestart()
        Toast.makeText(applicationContext, "OnRestart", Toast.LENGTH_LONG).show()
    }


    override fun onResume() {
        super.onResume()
        Toast.makeText(applicationContext, "OnResume", Toast.LENGTH_LONG).show()
    }


}
